<?php

namespace Drupal\checkpost;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Checkpost Middleware.
 */
class CheckpostMiddleware implements HttpKernelInterface {

  /**
   * The decorated kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The EntityTypeManager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * An alias manager to find the alias for the current system path.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Construct for the middleware.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The EntityTypeManager service.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher service.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   An alias manager to find the alias for the current system path.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   */
  public function __construct(HttpKernelInterface $http_kernel, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, PathMatcherInterface $path_matcher, AliasManagerInterface $alias_manager, CurrentPathStack $current_path) {
    $this->httpKernel = $http_kernel;
    $this->configFactory = $config_factory->get('checkpost.settings');
    $this->entityTypeManager = $entity_type_manager;
    $this->pathMatcher = $path_matcher;
    $this->aliasManager = $alias_manager;
    $this->currentPath = $current_path;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MAIN_REQUEST, $catch = TRUE): Response {
    $headers = unserialize($this->configFactory->get('headers')) ?? [];
    $pages = $this->configFactory->get('pages') ?? [];
    $source_ips = $this->configFactory->get('ips') ?? [];
    $source_cidrs = $this->configFactory->get('cidrs') ?? [];
    if (!$this->configFactory->get('enabled')) {
      return $this->httpKernel->handle($request, $type, $catch);
    }
    foreach ($pages as $page) {
      if ($this->checkPath($page, $request)) {
        return $this->httpKernel->handle($request, $type, $catch);
      }
    }
    foreach ($headers as $header) {
      if ((empty($header['value']) && $request->headers->has($header['name']))
        || !empty($header['value']) && $request->headers->get($header['name']) == $header['value']) {
        return $this->httpKernel->handle($request, $type, $catch);
      }
    }
    $ips = $request->getClientIps();
    foreach ($ips as $ip) {
      if (empty($source_ips) || $this->findIpFast($ip, $source_ips)) {
        return $this->httpKernel->handle($request, $type, $catch);
      }
      if (empty($source_cidrs) || $this->findCidr($ip, $source_cidrs)) {
        return $this->httpKernel->handle($request, $type, $catch);
      }
    }

    // All other requests will be blocked.
    return new Response('Access Denied', Response::HTTP_FORBIDDEN);
  }

  /**
   * Check whether the path is ignored.
   *
   * @param string $page
   *   The pages string/wildcards.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return bool
   *   TRUE/FALSE.
   */
  public function checkPath($page, Request $request) {
    // Convert path to lowercase. This allows comparison of the same path
    // with different case. Ex: /Page, /page, /PAGE.
    $pages = rtrim(trim(mb_strtolower($page)), '/');
    if (!$pages) {
      return FALSE;
    }
    // Compare the lowercase path alias (if any) and internal path.
    $path = $this->currentPath->getPath($request);
    // Do not trim a trailing slash if that is the complete path.
    $path = $path === '/' ? $path : rtrim($path, '/');
    $path_alias = mb_strtolower($this->aliasManager->getAliasByPath($path));

    return $this->pathMatcher->matchPath($path_alias, $pages) || (($path != $path_alias) && $this->pathMatcher->matchPath($path, $pages));
  }

  /**
   * Find IP address fast.
   *
   * @param string $cip
   *   The CIDR address.
   * @param array $ips
   *   The array of ip addresses.
   *
   * @return bool
   *   TRUE if IP address is found.
   */
  private function findIpFast(string $cip, array $ips) {
    $low = 0;
    $high = count($ips) - 1;

    while ($low <= $high) {
      $mid = floor(($low + $high) / 2);
      if ($ips[$mid] == $cip) {
        return TRUE;
      }
      if ($cip < $ips[$mid]) {
        $high = $mid - 1;
      }
      else {
        $low = $mid + 1;
      }
    }

    return FALSE;
  }

  /**
   * Find CIDR addresses.
   *
   * @param string $cip
   *   The CIDR IP string.
   * @param array $cidrs
   *   The CIDR addresses.
   *
   * @return bool
   *   TRUE if CIDR address is found.
   */
  private function findCidr(string $cip, array $cidrs) {
    foreach ($cidrs as $cidr) {
      if ($this->cidrCheck($cip, $cidr)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Function to expand and check CIDR address.
   *
   * @param string $ip
   *   The IP address to validate against.
   * @param string $cidr
   *   The CIDR IP.
   *
   * @return bool
   *   TRUE is IP address is found.
   */
  private function cidrCheck(string $ip, string $cidr) {
    [$net, $mask] = explode("/", $cidr);
    $ip_net = ip2long($net);
    $ip_mask = ~((1 << (32 - $mask)) - 1);
    $ip_ip = ip2long($ip);
    $ip_ip_net = $ip_ip & $ip_mask;
    return ($ip_ip_net == $ip_net);
  }

}
