<?php

namespace Drupal\checkpost\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings for checking access.
 *
 * Specify the pages you want to exclude, and
 * the headers to verify.
 */
class CheckpostSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'checkpost_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('checkpost.settings');

    $headers = !empty($config->get('headers')) ? unserialize($config->get('headers')) : [];

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable checkpost'),
      '#value' => $config->get('enabled'),
    ];

    $form['pages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Ignore pages'),
      '#value' => $config->get('pages') ? implode("\n", $config->get('pages')) : '',
      '#description' => $this->t("Specify pages to exclude the check by using their paths.
        Enter one path per line. The '*' character is a wildcard. An example path is %user-wildcard for every user page.
        %front is the front page.", [
          '%user-wildcard' => '/user/*',
          '%front' => '<front>',
        ]),
    ];

    $form['sources'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Ignore source IPs'),
      '#value' => ($config->get('ips') || $config->get('cidrs')) ? implode("\n", array_merge($config->get('ips'), $config->get('cidrs'))) : '',
      '#description' => $this->t("Specify IP address to exclude the check.
        Enter one ip per line. An example IP is %lambda to allow all requests from this IP.
        You can also have a CIDR notation for an IP address like this %cidr", [
          '%lambda' => '212.10.77.111',
          '%cidr' => '212.10.77.111/24',
        ]),
    ];

    $fieldName = 'header_fieldset';
    // Gather the number of names in the form already.
    $total_headers = $form_state->get('total_' . $fieldName);
    // We have to ensure that there is at least one name field.
    if ($total_headers === NULL) {
      $total_headers = count($headers) ? count($headers) : 1;
      $form_state->set('total_' . $fieldName, $total_headers);
    }

    $form['#tree'] = TRUE;
    $form[$fieldName] = [
      '#type' => 'details',
      '#id' => 'header-fieldset-wrapper',
      '#open' => TRUE,
      '#title' => $this->t('Request Headers'),
    ];

    for ($i = 0; $i < $total_headers; $i++) {
      $form[$fieldName][$i]['wrap'] = [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => $this->t("Header # %index", ['%index' => $i + 1]),
      ];

      $form[$fieldName][$i]['wrap']['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Header Name'),
        '#value' => $headers[$i]['name'] ?? '',
      ];

      $form[$fieldName][$i]['wrap']['value'] = [
        '#type' => 'textfield',
        '#value' => $headers[$i]['value'] ?? '',
        '#title' => $this->t('Header Value'),
        '#description' => $this->t('If the header value is empty, checkpost will only check if the header is present.'),
      ];
    }

    $form[$fieldName]['actions'] = [
      '#type' => 'actions',
    ];

    $form[$fieldName]['actions']['add_name'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'header-fieldset-wrapper',
        'target' => $fieldName,
      ],
    ];
    // If there is more than one name, add the remove button.
    if ($total_headers > 1) {
      $form[$fieldName]['actions']['remove_name'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'header-fieldset-wrapper',
          'target' => $fieldName,
        ],
      ];
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Get triggering element.
   */
  protected function triggeringElementFinder($form_state) {
    $triggeringElement = $form_state->getTriggeringElement();

    return ($triggeringElement['#ajax']['target'] ?? 'header');
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {

    $item = $this->triggeringElementFinder($form_state);

    return $form[$item];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $item = $this->triggeringElementFinder($form_state);
    $name_field = $form_state->get('total_' . $item);
    $add_button = $name_field + 1;
    $form_state->set('total_' . $item, $add_button);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $item = $this->triggeringElementFinder($form_state);
    $name_field = $form_state->get('total_' . $item);
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('total_' . $item, $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $userInputs = $form_state->getUserInput();
    $headers = $userInputs['header_fieldset'] ?? [];
    $pages = $userInputs['pages'] ?? '';
    $sources = $userInputs['sources'] ?? '';
    $sources = explode("\n", $sources);
    $sources = array_map(function ($source) {
      return trim($source);
    }, $sources);
    $sources = array_unique($sources);
    $ips = array_filter($sources, function ($source) {
      return strpos($source, '/') === FALSE;
    });
    sort($ips);
    $cidrs = array_diff($sources, $ips);
    sort($cidrs);
    $enabled = $userInputs['enabled'] ?? '';

    $header_value = [];
    foreach ($headers as $header) {
      if (isset($header['wrap'])) {
        if (!(empty($header['wrap']['value']) && empty($header['wrap']['name']))) {
          $header_value[] = array_filter($header['wrap']);
        }
      }
    }

    $this->config('checkpost.settings')
      ->set('headers', serialize($header_value))
      ->set('pages', explode("\n", $pages))
      ->set('ips', $ips)
      ->set('cidrs', $cidrs)
      ->set('enabled', $enabled)
      ->save();

  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'checkpost.settings',
    ];
  }

}
