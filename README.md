# CONTENTS OF THIS FILE

---------------------
* Introduction
* Installation and Use

## INTRODUCTION

------------

This is a module like [Shield][https://www.drupal.org/project/shield] to protect your dev sites.
But instead of http authentication, we use header based access checking. If your request contains a predefined header
and value, then the site will be accessible.

## INSTALLATION and USE

------------

This module has no other dependencies and is quite small. Just enable it and add your checks in the configuration page at
/admin/config/development/checkpost.
You can add pages you want to allow free access to in the "Ignore Pages" form field.
Add as many header values as you want, if any one of the header values are present in the requests to the website, you
will be able access the site. You can use a browser extension to add custom headers to the site like [Modheader][https://modheader.com/]
